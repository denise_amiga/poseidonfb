﻿module layouts.toolbar;

private import iup.iup, iup.iup_scintilla;
private import global, actionManager, menu, executer, dialogs.argOptionDlg;
private import Util = tango.text.Util, tango.stdc.stringz;


class CToolBar
{
	private:
	import 				iup.iup_scintilla;
	import 				menu, parser.ast, tools;
	
	Ihandle*			handle, listHandle, guiButton, bitButton;
	Ihandle* 			btnNew, btnOpen;
	Ihandle* 			btnSave, btnSaveAll;
	Ihandle* 			btnUndo, btnRedo, btnClearUndoBuffer;
	Ihandle* 			btnCut, btnCopy, btnPaste;
	Ihandle* 			btnBackNav, btnForwardNav, btnClearNav;
	Ihandle* 			btnMark, btnMarkPrev, btnMarkNext, btnMarkClean;
	Ihandle* 			btnCompile, btnBuildRun, btnRun, btnBuildAll, btnReBuild, btnQuickRun;
	Ihandle*[7]			labelSEPARATOR;

	void createToolBar()
	{
		btnNew				= IupButton( null, null );
		btnOpen				= IupButton( null, "Open" );

		btnSave				= IupButton( null, "Save" );
		btnSaveAll			= IupButton( null, "SaveAll" );	

		btnUndo				= IupButton( null, "Undo" );
		btnRedo				= IupButton( null, "Redo" );
		btnClearUndoBuffer	= IupButton( null, "Clear" );
		
		btnCut				= IupButton( null, "Cut" );
		btnCopy				= IupButton( null, "Copy" );
		btnPaste			= IupButton( null, "Paste" );

		btnBackNav			= IupButton( null, "Back" );
		btnForwardNav		= IupButton( null, "Forward" );
		btnClearNav			= IupButton( null, "Clear" );

		btnMark				= IupButton( null, "Mark" );
		btnMarkPrev			= IupButton( null, "MarkPrev" );
		btnMarkNext			= IupButton( null, "MarkNext" );
		btnMarkClean		= IupButton( null, "MarkClean" );
		
		btnCompile			= IupButton( null, "Compile" );
		btnBuildRun 		= IupButton( null, "BuildRun" );
		btnRun				= IupButton( null, "Run" );
		btnBuildAll			= IupButton( null, "BuildAll" );
		btnReBuild			= IupButton( null, "ReBuild" );
		btnQuickRun			= IupButton( null, "QuickRun" );


		IupSetAttributes( btnNew, "ALIGNMENT=ACENTER:ACENTER,FLAT=YES,IMAGE=icon_newfile,NAME=POSEIDON_TOOLBAR_New" );
		IupSetAttribute( btnNew, "TIP", GLOBAL.languageItems["caption_new"].toCString );
		IupSetCallback( btnNew, "ACTION", cast(Icallback) &menu.newFile_cb ); // From menu.d
		
		IupSetAttributes( btnOpen, "ALIGNMENT=ACENTER:ACENTER,FLAT=YES,IMAGE=icon_openfile,NAME=POSEIDON_TOOLBAR_Open" );
		IupSetAttribute( btnOpen, "TIP", GLOBAL.languageItems["caption_open"].toCString );
		IupSetCallback( btnOpen, "ACTION", cast(Icallback) &menu.openFile_cb ); // From menu.d

		IupSetAttributes( btnSave, "ALIGNMENT=ACENTER:ACENTER,FLAT=YES,IMAGE=icon_save,NAME=POSEIDON_TOOLBAR_Save" );
		IupSetAttribute( btnSave, "TIP", GLOBAL.languageItems["sc_save"].toCString );
		IupSetCallback( btnSave, "ACTION", cast(Icallback) &menu.saveFile_cb ); // From menu.d

		IupSetAttributes( btnSaveAll, "ALIGNMENT=ACENTER:ACENTER,FLAT=YES,IMAGE=icon_saveall,NAME=POSEIDON_TOOLBAR_SaveAll" );
		IupSetAttribute( btnSaveAll, "TIP", GLOBAL.languageItems["sc_saveall"].toCString );
		IupSetCallback( btnSaveAll, "ACTION", cast(Icallback) &menu.saveAllFile_cb ); // From menu.d

		IupSetAttributes( btnUndo, "ALIGNMENT=ACENTER:ACENTER,FLAT=YES,IMAGE=icon_undo,ACTIVE=NO,NAME=POSEIDON_TOOLBAR_Undo" );
		IupSetAttribute( btnUndo, "TIP", GLOBAL.languageItems["sc_undo"].toCString );
		IupSetCallback( btnUndo, "ACTION", cast(Icallback) &menu.undo_cb ); // From menu.d
		
		IupSetAttributes( btnRedo, "ALIGNMENT=ACENTER:ACENTER,FLAT=YES,IMAGE=icon_redo,ACTIVE=NO,NAME=POSEIDON_TOOLBAR_Redo" );
		IupSetAttribute( btnRedo, "TIP", GLOBAL.languageItems["sc_redo"].toCString );
		IupSetCallback( btnRedo, "ACTION", cast(Icallback) &menu.redo_cb ); // From menu.d
		
		IupSetAttributes( btnClearUndoBuffer, "ALIGNMENT=ACENTER:ACENTER,FLAT=YES,IMAGE=icon_clear,NAME=POSEIDON_TOOLBAR_ClearUndoBuffer" );
		IupSetAttribute( btnClearUndoBuffer, "TIP", GLOBAL.languageItems["clear"].toCString );
		IupSetCallback( btnClearUndoBuffer, "ACTION", cast(Icallback) function()
		{
			auto sci = ScintillaAction.getActiveIupScintilla;
			if( sci != null ) IupScintillaSendMessage( sci, 2175, 0, 0 ); // SCI_EMPTYUNDOBUFFER 2175
			
			Ihandle* _undo = IupGetDialogChild( GLOBAL.toolbar.getHandle, "POSEIDON_TOOLBAR_Undo" );
			if( _undo != null ) IupSetAttribute( _undo, "ACTIVE", "NO" ); // SCI_CANUNDO 2174

			Ihandle* _redo = IupGetDialogChild( GLOBAL.toolbar.getHandle, "POSEIDON_TOOLBAR_Redo" );
			if( _redo != null ) IupSetAttribute( _redo, "ACTIVE", "NO" ); // SCI_CANREDO 2016
			
			DocumentTabAction.setFocus( sci );
			return IUP_DEFAULT;
		});		
		

		IupSetAttributes( btnCut, "ALIGNMENT=ACENTER:ACENTER,FLAT=YES,IMAGE=icon_cut,NAME=POSEIDON_TOOLBAR_Cut" );
		IupSetAttribute( btnCut, "TIP", GLOBAL.languageItems["caption_cut"].toCString );
		IupSetCallback( btnCut, "ACTION", cast(Icallback) &menu.cut_cb ); // From menu.d
		
		IupSetAttributes( btnCopy, "ALIGNMENT=ACENTER:ACENTER,FLAT=YES,IMAGE=icon_copy,NAME=POSEIDON_TOOLBAR_Copy" );
		IupSetAttribute( btnCopy, "TIP", GLOBAL.languageItems["caption_copy"].toCString );
		IupSetCallback( btnCopy, "ACTION", cast(Icallback) &menu.copy_cb ); // From menu.d
		
		IupSetAttributes( btnPaste, "ALIGNMENT=ACENTER:ACENTER,FLAT=YES,IMAGE=icon_paste,NAME=POSEIDON_TOOLBAR_Paste" );
		IupSetAttribute( btnPaste, "TIP", GLOBAL.languageItems["caption_paste"].toCString );
		IupSetCallback( btnPaste, "ACTION", cast(Icallback) &menu.paste_cb ); // From menu.d



		// Nav
		IupSetAttributes( btnBackNav, "ALIGNMENT=ACENTER:ACENTER,FLAT=YES,IMAGE=icon_debug_left,ACTIVE=NO,NAME=POSEIDON_TOOLBAR_BackwardNav" );
		IupSetAttribute( btnBackNav, "TIP", GLOBAL.languageItems["sc_backnav"].toCString );
		IupSetHandle( "toolbar_BackNav", btnBackNav );
		IupSetCallback( btnBackNav, "ACTION", cast(Icallback) function()
		{
			auto cacheUnit = GLOBAL.navigation.back();
			if( cacheUnit._line != -1 ) ScintillaAction.openFile( cacheUnit._fullPath, cacheUnit._line );
			return IUP_DEFAULT;
		});		
		
		IupSetAttributes( btnForwardNav, "ALIGNMENT=ACENTER:ACENTER,FLAT=YES,IMAGE=icon_debug_right,ACTIVE=NO,NAME=POSEIDON_TOOLBAR_ForwardNav" );
		IupSetAttribute( btnForwardNav, "TIP", GLOBAL.languageItems["sc_forwardnav"].toCString );
		IupSetHandle( "toolbar_ForwardNav", btnForwardNav );
		IupSetCallback( btnForwardNav, "ACTION", cast(Icallback) function()
		{
			auto cacheUnit = GLOBAL.navigation.forward();
			if( cacheUnit._line != -1 ) ScintillaAction.openFile( cacheUnit._fullPath, cacheUnit._line );
			return IUP_DEFAULT;
		});			
		
		IupSetAttributes( btnClearNav, "ALIGNMENT=ACENTER:ACENTER,FLAT=YES,IMAGE=icon_clear,NAME=POSEIDON_TOOLBAR_ClearNav" );
		IupSetAttribute( btnClearNav, "TIP", GLOBAL.languageItems["clear"].toCString );
		IupSetHandle( "toolbar_ClearNav", btnClearNav );
		IupSetCallback( btnClearNav, "ACTION", cast(Icallback) function()
		{
			GLOBAL.navigation.clear();
			return IUP_DEFAULT;
		});


		IupSetAttributes( btnMark, "ALIGNMENT=ACENTER:ACENTER,FLAT=YES,IMAGE=icon_mark,NAME=POSEIDON_TOOLBAR_Mark" );
		IupSetAttribute( btnMark, "TIP", GLOBAL.languageItems["bookmark"].toCString );
		IupSetCallback( btnMark, "ACTION", cast(Icallback) function()
		{
			Ihandle* ih = actionManager.ScintillaAction.getActiveIupScintilla();
			if( ih != null )
			{
				int currentPos			= cast(int) IupScintillaSendMessage( ih, 2008, 0, 0 ); // SCI_GETCURRENTPOS = 2008
				int currentLine  		= cast(int) IupScintillaSendMessage( ih, 2166, currentPos, 0 ); // SCI_LINEFROMPOSITION = 2166
				
				if( IupGetIntId( ih, "MARKERGET", currentLine ) & 2 )
				{
					IupSetIntId( ih, "MARKERDELETE", currentLine, 1 );
				}
				else
				{
					IupSetIntId( ih, "MARKERADD", currentLine, 1 );
				}
			}
			return IUP_DEFAULT;
		});
		
		IupSetAttributes( btnMarkPrev, "ALIGNMENT=ACENTER:ACENTER,FLAT=YES,IMAGE=icon_markprev,NAME=POSEIDON_TOOLBAR_MarkPrev" );
		IupSetAttribute( btnMarkPrev, "TIP", GLOBAL.languageItems["bookmarkprev"].toCString );
		IupSetCallback( btnMarkPrev, "ACTION", cast(Icallback) function()
		{
			Ihandle* ih = actionManager.ScintillaAction.getActiveIupScintilla();
			if( ih != null )
			{
				int currentPos			= cast(int) IupScintillaSendMessage( ih, 2008, 0, 0 ); // SCI_GETCURRENTPOS = 2008
				int currentLine  		= cast(int) IupScintillaSendMessage( ih, 2166, currentPos, 0 ); // SCI_LINEFROMPOSITION = 2166

				IupSetIntId( ih, "MARKERPREVIOUS", --currentLine, 2 );
				
				int markLineNumber 		= IupGetInt( ih, "LASTMARKERFOUND" );

				if( markLineNumber < 0 )
				{
					int count = IupGetInt( ih, "LINECOUNT" );
					IupSetIntId( ih, "MARKERPREVIOUS", count, 2 );
					markLineNumber = IupGetInt( ih, "LASTMARKERFOUND" );
					if( markLineNumber < 0 ) return IUP_DEFAULT;
				}			
				
				IupSetFocus( ih );
				if( markLineNumber > -1 )
				{
					IupSetAttributeId( ih, "ENSUREVISIBLE", markLineNumber, "ENFORCEPOLICY" );
					IupSetInt( ih, "CARET", markLineNumber );				
				}
				StatusBarAction.update();
			}
			
			return IUP_DEFAULT;
		});
		
		IupSetAttributes( btnMarkNext, "ALIGNMENT=ACENTER:ACENTER,FLAT=YES,IMAGE=icon_marknext,NAME=POSEIDON_TOOLBAR_MarkNext" );
		IupSetAttribute( btnMarkNext, "TIP", GLOBAL.languageItems["bookmarknext"].toCString );
		IupSetCallback( btnMarkNext, "ACTION", cast(Icallback) function()
		{
			Ihandle* ih = actionManager.ScintillaAction.getActiveIupScintilla();
			if( ih != null )
			{
				int currentPos			= cast(int) IupScintillaSendMessage( ih, 2008, 0, 0 ); // SCI_GETCURRENTPOS = 2008
				int currentLine  		= cast(int) IupScintillaSendMessage( ih, 2166, currentPos, 0 ); // SCI_LINEFROMPOSITION = 2166

				IupSetIntId( ih, "MARKERNEXT", ++currentLine, 2 );
				
				int markLineNumber 		= IupGetInt( ih, "LASTMARKERFOUND" );

				if( markLineNumber < 0 )
				{
					IupSetIntId( ih, "MARKERNEXT", 0, 2 );
					markLineNumber = IupGetInt( ih, "LASTMARKERFOUND" );
					if( markLineNumber < 0 ) return IUP_DEFAULT;
				}

				IupSetFocus( ih );
				IupSetAttributeId( ih, "ENSUREVISIBLE", markLineNumber, "ENFORCEPOLICY" );
				IupSetInt( ih, "CARET", markLineNumber );
				StatusBarAction.update();
			}
			
			return IUP_DEFAULT;
		});
		
		IupSetAttributes( btnMarkClean, "ALIGNMENT=ACENTER:ACENTER,FLAT=YES,IMAGE=icon_markclear,NAME=POSEIDON_TOOLBAR_MarkClear" );
		IupSetAttribute( btnMarkClean, "TIP", GLOBAL.languageItems["bookmarkclear"].toCString );
		IupSetCallback( btnMarkClean, "ACTION", cast(Icallback) function()
		{
			Ihandle* ih = actionManager.ScintillaAction.getActiveIupScintilla();
			if( ih != null ) IupSetInt( ih, "MARKERDELETEALL", 1 );
			return IUP_DEFAULT;
		});


		IupSetAttributes( btnCompile, "ALIGNMENT=ACENTER:ACENTER,FLAT=YES,IMAGE=icon_compile,NAME=POSEIDON_TOOLBAR_Compile" );
		IupSetAttribute( btnCompile, "TIP", GLOBAL.languageItems["sc_compile"].toCString );
		IupSetCallback( btnCompile, "BUTTON_CB", cast(Icallback) &compile_button_cb );

		IupSetAttributes( btnBuildRun, "ALIGNMENT=ACENTER:ACENTER,FLAT=YES,IMAGE=icon_buildrun,NAME=POSEIDON_TOOLBAR_CompileRun" );
		IupSetAttribute( btnBuildRun, "TIP", GLOBAL.languageItems["sc_compilerun"].toCString );
		IupSetCallback( btnBuildRun, "BUTTON_CB", cast(Icallback) &buildrun_button_cb );

		IupSetAttributes( btnRun, "ALIGNMENT=ACENTER:ACENTER,FLAT=YES,IMAGE=icon_run,NAME=POSEIDON_TOOLBAR_Run" );
		IupSetAttribute( btnRun, "TIP", GLOBAL.languageItems["sc_run"].toCString );
		IupSetCallback( btnRun, "BUTTON_CB", cast(Icallback) &run_button_cb );

		IupSetAttributes( btnBuildAll, "ALIGNMENT=ACENTER:ACENTER,FLAT=YES,IMAGE=icon_build,NAME=POSEIDON_TOOLBAR_Build" );
		IupSetAttribute( btnBuildAll, "TIP", GLOBAL.languageItems["sc_build"].toCString );
		IupSetCallback( btnBuildAll, "BUTTON_CB", cast(Icallback) &build_button_cb );
		IupSetHandle( "toolbar_BuildAll", btnBuildAll );
		
		IupSetAttributes( btnReBuild, "ALIGNMENT=ACENTER:ACENTER,FLAT=YES,IMAGE=icon_rebuild,NAME=POSEIDON_TOOLBAR_ReBuild" );
		IupSetAttribute( btnReBuild, "TIP", GLOBAL.languageItems["rebuildprj"].toCString );
		IupSetCallback( btnReBuild, "BUTTON_CB", cast(Icallback) &buildall_button_cb );
		IupSetHandle( "toolbar_ReBuild", btnReBuild );

		IupSetAttributes( btnQuickRun, "ALIGNMENT=ACENTER:ACENTER,FLAT=YES,IMAGE=icon_quickrun,NAME=POSEIDON_TOOLBAR_QuickRun" );
		IupSetAttribute( btnQuickRun, "TIP", GLOBAL.languageItems["sc_quickrun"].toCString );
		IupSetCallback( btnQuickRun, "BUTTON_CB", cast(Icallback) &quickRun_button_cb );


		
		for( int i = 0; i < 7; i++ )
		{
			labelSEPARATOR[i] = IupFlatSeparator(); 
			IupSetAttributes( labelSEPARATOR[i], "STYLE=EMPTY" );
		}

		version(Windows)
		{
			guiButton = IupToggle( null, "GUI" );
			IupSetAttributes( guiButton, "ALIGNMENT=ACENTER:ACENTER,FLAT=YES,IMAGE=icon_console,IMPRESS=icon_gui,NAME=POSEIDON_TOOLBAR_Gui" );
			IupSetAttribute( guiButton, "TIP", "Console / GUI" );
			if( GLOBAL.editorSetting00.GUI == "OFF" ) IupSetAttribute( guiButton, "VALUE", "OFF" ); else IupSetAttribute( guiButton, "VALUE", "ON" );
			IupSetCallback( guiButton, "ACTION", cast(Icallback) function( Ihandle* ih )
			{
				if( GLOBAL.editorSetting00.GUI == "ON" ) GLOBAL.editorSetting00.GUI = "OFF"; else GLOBAL.editorSetting00.GUI = "ON";
				return IUP_DEFAULT;
			});			
			
			bitButton = IupToggle( null, "bit" );
			IupSetAttributes( bitButton, "ALIGNMENT=ACENTER:ACENTER,FLAT=YES,IMAGE=icon_32,IMPRESS=icon_64,NAME=POSEIDON_TOOLBAR_Bit" );
			IupSetAttribute( bitButton, "TIP", "32 / 64 bit" );
			if( GLOBAL.editorSetting00.Bit64 == "OFF" ) IupSetAttribute( bitButton, "VALUE", "OFF" ); else IupSetAttribute( bitButton, "VALUE", "ON" );
			IupSetCallback( bitButton, "ACTION", cast(Icallback) function( Ihandle* ih )
			{
				if( GLOBAL.editorSetting00.Bit64 == "ON" ) GLOBAL.editorSetting00.Bit64 = "OFF"; else GLOBAL.editorSetting00.Bit64 = "ON";
				return IUP_DEFAULT;
			});
		}
		
		listHandle = IupFlatList();
		IupSetAttributes( listHandle, "SIZE=x12,ACTIVE=YES,SHOWIMAGE=YES,SCROLLBAR=NO,NAME=POSEIDON_TOOLBAR_FunctionList" );
		version(Windows) IupSetStrAttribute( listHandle, "FONT", toStringz( GLOBAL.fonts[0].fontString ) ); else IupSetStrAttribute( listHandle, "FONTFACE", toStringz( GLOBAL.fonts[0].fontString ) );
		IupSetAttribute( listHandle, "EXPAND", "HORIZONTAL" );
		IupSetStrAttribute( listHandle, "FGCOLOR", GLOBAL.editColor.txtFore.toCString );
		IupSetStrAttribute( listHandle, "HLCOLORALPHA", "0" );
		IupSetStrAttribute( listHandle, "BGCOLOR", GLOBAL.editColor.txtBack.toCString );
		if( GLOBAL.showFunctionTitle == "ON" ) IupSetAttribute( listHandle, "VISIBLE", "YES" ); else IupSetAttribute( listHandle, "VISIBLE", "NO" );
		
		Ihandle* commandText = IupScintilla();
		IupSetAttributes( commandText, "ACTIVE=YES,NAME=POSEIDON_COMMANDLINE,VISIBLE=NO,SIZE=1x8" );
		IupSetCallback( commandText, "SAVEPOINT_CB", cast(Icallback) &command_SAVEPOINT_CB );
		
		
		// IUP Container to put buttons on~
		version(Windows)
		{
			handle = IupHbox( btnNew, btnOpen, labelSEPARATOR[0], btnSave, btnSaveAll, labelSEPARATOR[3], btnUndo, btnRedo, btnClearUndoBuffer, labelSEPARATOR[1], btnCut, btnCopy, btnPaste, labelSEPARATOR[6], btnBackNav, btnForwardNav, btnClearNav, labelSEPARATOR[2], btnMark, btnMarkPrev,
					btnMarkNext, btnMarkClean, labelSEPARATOR[4], btnCompile, btnBuildRun, btnRun, btnBuildAll, btnReBuild, btnQuickRun, labelSEPARATOR[5], bitButton, guiButton, listHandle, commandText, null );
		}
		else
		{
			handle = IupHbox( btnNew, btnOpen, labelSEPARATOR[0], btnSave, btnSaveAll, labelSEPARATOR[3], btnUndo, btnRedo, btnClearUndoBuffer, labelSEPARATOR[1], btnCut, btnCopy, btnPaste, labelSEPARATOR[6], btnBackNav, btnForwardNav, btnClearNav, labelSEPARATOR[2], btnMark, btnMarkPrev,
					btnMarkNext, btnMarkClean, labelSEPARATOR[4], btnCompile, btnBuildRun, btnRun, btnBuildAll, btnReBuild, btnQuickRun, listHandle, commandText, null );
		}

		handle = IupBackgroundBox( handle );
		version(Windows) IupSetStrAttribute( handle, "BGCOLOR", GLOBAL.editColor.dlgBack.toCString ); // linux get IupFlatSeparator wrong color

		IupSetAttributes( handle, "GAP=2,ALIGNMENT=ACENTER,NAME=POSEIDON_TOOLBAR" );
		version(linux) IupSetAttributes( handle, "MARGIN=0x2" );
		
		changeIcons();
	}
	
	void changeIcons()
	{
		char[] tail;
		if( GLOBAL.editorSetting00.IconInvert == "ON" ) tail = "_invert"; else return;

		IupSetStrAttribute( btnNew, "IMAGE", toStringz( "icon_newfile" ~ tail ) );
		IupSetStrAttribute( btnOpen, "IMAGE", toStringz( "icon_openfile" ~ tail ) );
		IupSetStrAttribute( btnSave, "IMAGE", toStringz( "icon_save" ~ tail ) );
		IupSetStrAttribute( btnSaveAll, "IMAGE", toStringz( "icon_saveall" ~ tail ) );
		IupSetStrAttribute( btnUndo, "IMAGE", toStringz( "icon_undo" ~ tail ) );
		IupSetStrAttribute( btnRedo, "IMAGE", toStringz( "icon_redo" ~ tail ) );
		IupSetStrAttribute( btnClearUndoBuffer, "IMAGE", toStringz( "icon_clear" ~ tail ) );

		IupSetStrAttribute( btnCut, "IMAGE", toStringz( "icon_cut" ~ tail ) );
		IupSetStrAttribute( btnCopy, "IMAGE", toStringz( "icon_copy" ~ tail ) );
		IupSetStrAttribute( btnPaste, "IMAGE", toStringz( "icon_paste" ~ tail ) );

		IupSetStrAttribute( btnBackNav, "IMAGE", toStringz( "icon_debug_left" ~ tail ) );
		IupSetStrAttribute( btnForwardNav, "IMAGE", toStringz( "icon_debug_right" ~ tail ) );
		IupSetStrAttribute( btnClearNav, "IMAGE", toStringz( "icon_clear" ~ tail ) );

		IupSetStrAttribute( btnMark, "IMAGE", toStringz( "icon_mark" ~ tail ) );
		IupSetStrAttribute( btnMarkPrev, "IMAGE", toStringz( "icon_markprev" ~ tail ) );
		IupSetStrAttribute( btnMarkNext, "IMAGE", toStringz( "icon_marknext" ~ tail ) );
		IupSetStrAttribute( btnMarkClean, "IMAGE", toStringz( "icon_markclear" ~ tail ) );

		IupSetStrAttribute( btnCompile, "IMAGE", toStringz( "icon_compile" ~ tail ) );
		IupSetStrAttribute( btnBuildRun, "IMAGE", toStringz( "icon_buildrun" ~ tail ) );
		IupSetStrAttribute( btnRun, "IMAGE", toStringz( "icon_run" ~ tail ) );
		IupSetStrAttribute( btnBuildAll, "IMAGE", toStringz( "icon_build" ~ tail ) );
		IupSetStrAttribute( btnReBuild, "IMAGE", toStringz( "icon_rebuild" ~ tail ) );
		IupSetStrAttribute( btnQuickRun, "IMAGE", toStringz( "icon_quickrun" ~ tail ) );

		IupSetStrAttribute( guiButton, "IMAGE", toStringz( "icon_console" ~ tail ) );
		IupSetStrAttribute( guiButton, "IMPRESS", toStringz( "icon_gui" ~ tail ) );
		IupSetStrAttribute( bitButton, "IMAGE", toStringz( "icon_32" ~ tail ) );
		IupSetStrAttribute( bitButton, "IMPRESS", toStringz( "icon_64" ~ tail ) );
	}


	public:
	this()
	{
		createToolBar();
	}
	
	~this()
	{
	}

	Ihandle* getHandle()
	{
		return handle;
	}

	Ihandle* getListHandle()
	{
		return listHandle;
	}

	Ihandle* getGuiButtonHandle()
	{
		return guiButton;
	}

	Ihandle* getBitButtonHandle()
	{
		return bitButton;
	}

	bool checkGuiButtonStatus()
	{
		if( fromStringz( IupGetAttribute( guiButton, "VALUE" ) ) == "ON" ) return true;
		return false;
	}
	
	void showList( bool status )
	{
		if( status ) IupSetAttribute( listHandle, "VISIBLE", "YES" ); else IupSetAttribute( listHandle, "VISIBLE", "NO" );
	}

	void changeColor()
	{
		version(Windows) IupSetStrAttribute( handle, "BGCOLOR", GLOBAL.editColor.dlgBack.toCString );
		
		IupSetStrAttribute( listHandle, "FGCOLOR", GLOBAL.editColor.txtFore.toCString );
		IupSetStrAttribute( listHandle, "HLCOLORALPHA", "0" );
		IupSetStrAttribute( listHandle, "BGCOLOR", GLOBAL.editColor.txtBack.toCString );
	}
}


extern( C )
{
	private int compile_button_cb( Ihandle* ih, int button, int pressed, int x, int y, char* status )
	{
		if( pressed == 0 )
		{
			if( button == IUP_BUTTON1 ) // Left Click
			{
				ExecuterAction.compile();
			}
			else if( button == IUP_BUTTON3 ) // Right Click
			{
				if( IupGetInt( GLOBAL.documentTabs, "COUNT" ) > 0 ) // No document, exit
				{
					auto dlg = new CArgOptionDialog( 480, -1, GLOBAL.languageItems["caption_argtitle"].toDString(), 1 );
					char[][] result = dlg.show( IUP_MOUSEPOS, IUP_MOUSEPOS, 1  );
					if( result.length == 3 ) ExecuterAction.compile( result[0], null, result[2] );
					delete dlg;
				}
			}
		}
		return IUP_DEFAULT;
	}
	
	private int build_button_cb( Ihandle* ih, int button, int pressed, int x, int y, char* status )
	{
		if( pressed == 0 )
		{
			if( button == IUP_BUTTON1 ) // Left Click
			{
				ExecuterAction.build();
			}
			else if( button == IUP_BUTTON3 ) // Right Click
			{
				if( IupGetInt( GLOBAL.documentTabs, "COUNT" ) > 0 ) // No document, exit
				{
					auto dlg = new CArgOptionDialog( 480, -1, GLOBAL.languageItems["caption_argtitle"].toDString(), 1 );
					char[][] result = dlg.show( IUP_MOUSEPOS, IUP_MOUSEPOS, 1  );
					if( result.length == 3 ) ExecuterAction.build( result[0], null, result[2] );
					delete dlg;
				}
			}
		}
		return IUP_DEFAULT;
	}
	
	private int buildall_button_cb( Ihandle* ih, int button, int pressed, int x, int y, char* status )
	{
		if( pressed == 0 )
		{
			if( button == IUP_BUTTON1 ) // Left Click
			{
				ExecuterAction.buildAll();
			}
			else if( button == IUP_BUTTON3 ) // Right Click
			{
				if( IupGetInt( GLOBAL.documentTabs, "COUNT" ) > 0 ) // No document, exit
				{
					auto dlg = new CArgOptionDialog( 480, -1, GLOBAL.languageItems["caption_argtitle"].toDString(), 1 );
					char[][] result = dlg.show( IUP_MOUSEPOS, IUP_MOUSEPOS, 1  );
					if( result.length == 3 ) ExecuterAction.buildAll( result[0], result[2], null );
					delete dlg;
				}
			}
		}
		return IUP_DEFAULT;
	}
	
	private int buildrun_button_cb( Ihandle* ih, int button, int pressed, int x, int y, char* status )
	{
		if( pressed == 0 )
		{
			if( button == IUP_BUTTON1 ) // Left Click
			{
				ExecuterAction.compile( null, null, null, null, true );
			}
			else if( button == IUP_BUTTON3 ) // Right Click
			{
				if( IupGetInt( GLOBAL.documentTabs, "COUNT" ) > 0 ) // No document, exit
				{
					auto dlg = new CArgOptionDialog( 480, -1, GLOBAL.languageItems["caption_argtitle"].toDString(), 3 );
					char[][] result = dlg.show( IUP_MOUSEPOS, IUP_MOUSEPOS, 3  ).dup;
					if( result.length == 3 ) ExecuterAction.compile( result[0], result[1], result[2], null, true ); // 3rd parameter = " " is compile & run
					delete dlg;
				}
			}
		}
		return IUP_DEFAULT;
	}	
	
	private int run_button_cb( Ihandle* ih, int button, int pressed, int x, int y, char* status )
	{
		if( pressed == 0 )
		{
			if( button == IUP_BUTTON1 ) // Left Click
			{
				ExecuterAction.run();
			}
			else if( button == IUP_BUTTON3 ) // Right Click
			{
				if( IupGetInt( GLOBAL.documentTabs, "COUNT" ) > 0 ) // No document, exit
				{
					auto dlg = new CArgOptionDialog( 480, -1, GLOBAL.languageItems["caption_argtitle"].toDString(), 2 );
					char[][] result = dlg.show( IUP_MOUSEPOS, IUP_MOUSEPOS, 2  );
					if( result.length == 3 ) ExecuterAction.run( result[1] );
					delete dlg;
				}
			}
		}
		return IUP_DEFAULT;
	}
	
	private int quickRun_button_cb( Ihandle* ih, int button, int pressed, int x, int y, char* status )
	{
		if( pressed == 0 )
		{
			if( button == IUP_BUTTON1 ) // Left Click
			{
				ExecuterAction.quickRun( /*Util.trim( GLOBAL.defaultOption.toDString )*/ );
			}
			else if( button == IUP_BUTTON3 ) // Right Click
			{
				if( IupGetInt( GLOBAL.documentTabs, "COUNT" ) > 0 ) // No document, exit
				{
					auto dlg = new CArgOptionDialog( 480, -1, GLOBAL.languageItems["caption_argtitle"].toDString(), 3 );
					char[][] result = dlg.show( IUP_MOUSEPOS, IUP_MOUSEPOS, 3  );
					if( result.length == 3 ) ExecuterAction.quickRun( result[0], result[1], result[2] );
					delete dlg;
				}
			}
		}
		return IUP_DEFAULT;
	}

	/*
	For plugins......
	*/
	private int command_SAVEPOINT_CB( Ihandle *ih, int status )
	{
		if( status == 0 )
		{
			IupScintillaSendMessage( ih, 2175, 0, 0 ); // SCI_EMPTYUNDOBUFFER = 2175
			
			char[] command = fromStringz( IupGetAttribute( ih, "VALUE" ) );
			if( command.length )
			{
				char[][] splitCommand = Util.split( command, "," );
				
				if( splitCommand.length == 1 )
				{
					auto cSci = actionManager.ScintillaAction.getActiveCScintilla();
					if( cSci !is null )
					{
						switch( command )
						{
							case "NewFile":
								return menu.newFile_cb( cSci.getIupScintilla );
								
							case "OpenFile":
								return menu.openFile_cb( cSci.getIupScintilla );
							
							case "SaveFile":
								return menu.saveFile_cb( cSci.getIupScintilla );
								
							case "SaveAs":
								return menu.saveAsFile_cb( cSci.getIupScintilla );
								
							case "CloseFile":
								actionManager.ScintillaAction.closeDocument( cSci.getFullPath() );
								return IUP_DEFAULT;	
								
							case "NewProject":
								return menu.newProject_cb( cSci.getIupScintilla );

							case "OpenProject":
								return menu.openProject_cb( cSci.getIupScintilla );
								
							case "CloseProject":
								return menu.closeProject_cb( cSci.getIupScintilla );
								
							case "CloseAllProject":
								return menu.closeAllProject_cb( cSci.getIupScintilla );

							case "SaveProject":
								return menu.saveProject_cb( cSci.getIupScintilla );

							case "SaveAllProject":
								return menu.saveAllProject_cb( cSci.getIupScintilla );
								
							case "ProjectProperties":
								return menu.projectProperties_cb( cSci.getIupScintilla );
								
							case "Compile":
								return menu.compile_cb( cSci.getIupScintilla );

							case "CompileRun":
								return menu.buildrun_cb( cSci.getIupScintilla );

							case "Run":
								return menu.run_cb( cSci.getIupScintilla );

							case "Build":
								return menu.buildAll_cb( cSci.getIupScintilla );

							case "ReBuild":
								return menu.reBuild_cb( cSci.getIupScintilla );

							case "QuickRun":
								return menu.quickRun_cb( cSci.getIupScintilla );
								
							case "Comment":
								return menu.comment_cb( null );

							case "UnComment":
								return menu.uncomment_cb( null );
								
							default:
						}
					}
				}
				else if( splitCommand.length == 2 )
				{
					switch( Util.trim( splitCommand[0] ) )
					{
						case "FBCx32":
							GLOBAL.compilerFullPath = Util.trim( splitCommand[1] ).dup;
							if( GLOBAL.preferenceDlg !is null ) IupSetAttribute( IupGetHandle( "compilerPath_Handle" ), "VALUE", toStringz( GLOBAL.compilerFullPath.dup ) );
							break;

						case "FBCx64":
							version(Windows)
							{
								GLOBAL.x64compilerFullPath = Util.trim( splitCommand[1] ).dup;
								if( GLOBAL.preferenceDlg !is null ) IupSetAttribute( IupGetHandle( "x64compilerPath_Handle" ), "VALUE", toStringz( GLOBAL.x64compilerFullPath.dup ) );
							}
							break;
						default:
					}
				}
			}
		}
		
		return IUP_DEFAULT;
	}	
}