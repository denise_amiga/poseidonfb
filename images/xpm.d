﻿module images.xpm;

struct XPM
{
	private:
	import iup.iup;
	
	import tango.io.device.File, tango.io.stream.Lines, Util = tango.text.Util, Integer = tango.text.convert.Integer, tools;
	import tango.stdc.stringz;
	import tango.io.Stdout, tango.stdc.math;

	import global;

	struct ColorUnit
	{
		char[] 	index, c, value;
		int		sn;
	}

	static ubyte hexStringToByte( char[] hex )
	{
		hex = lowerCase( hex );
	
		if( hex.length == 2 )
		{
			uint d1, d2;
			
			if( hex[0] == '0' )
			{
				d2 = 0;
			}
			else if( hex[0] >= 97 && hex[0] <= 102 )
			{
				d2 = ( hex[0] - 87 ) * 16;
			}
			else
			{
				d2 = ( hex[0] - 48 ) * 16;
			}

			if( hex[1] >= 97 && hex[1] <= 102 )
			{
				d1 = hex[1] - 87;
			}
			else if( hex[1] == '0' )
			{
				d1 = 0;
			}
			else
			{
				d1 = hex[1] - 48;
			}
		
			return cast(ubyte) ( d2 + d1 );
		}

		return 0;
	}

	static IupString convert( char[] filePath )
	{
		try
		{
			//return null;

			scope file = new File( filePath, File.ReadExisting );
			int 		quoteLineCount;
			int			width, height, num_colors, chars_per_pixel;
			int			rPos;
			bool 		bFormat, bPixel, bColor;
			
			char[]		pixel;
			ColorUnit[]	color;
			
			foreach( int count, char[] line; new Lines!(char)(file) )
			{
				if( count == 0 )
				{
					if( line != "/* XPM */" ) return null;
				}
				
				if( line.length > 2 )
				{
					if( line[0..2] == "/*" || line[0] != '"' ) continue;
					
					if( line[0] == '"' ) quoteLineCount++;
					
					if( quoteLineCount == 1 )
					{
						char[]	formatString;
						bool	bIsNumber;
						foreach( char c; line[1..$-2] )
						{
							if( c > 47 && c < 58 )
							{
								formatString ~= c;
								bIsNumber = true;
							}
							else
							{
								if( bIsNumber )
								{
									bIsNumber = false;
									formatString ~= " ";
								}
							}
						}
						
						char[][] splitWords = Util.split( formatString, " " );
						if( splitWords.length > 3 )
						{
							width = Integer.atoi( splitWords[0] );
							height = Integer.atoi( splitWords[1] );
							num_colors = Integer.atoi( splitWords[2] );
							chars_per_pixel = Integer.atoi( splitWords[3] );
						}
						continue;
					}
					else if( quoteLineCount <= num_colors + 1 )
					{
						rPos = Util.rindex( line, "\"" );
						char[][] splitData = Util.split( line[1..rPos], " " );
						if( splitData.length == 3 )
						{
							ColorUnit _color;
							_color.index = splitData[0].dup;
							_color.c = splitData[1].dup;
							if( splitData[2] == "None" )
							{
								_color.value = "00000000".dup;
							}
							else
							{
								_color.value = ( splitData[2][1..$] ~ "ff" ).dup;
								
							}

							color ~= _color;
						}					
					
					}
					else if( quoteLineCount <= num_colors + 1 + width )
					{
						rPos = Util.rindex( line, "\"" );
						foreach( char c; line[1..rPos] )
							pixel ~= c;
					}
				}
			}

			ubyte[] result; 

			foreach( char c; pixel )
			{
				foreach( ColorUnit _color; color )
				{
					if( c == _color.index[0] )
					{
						result ~= hexStringToByte( _color.value[0..2] );
						result ~= hexStringToByte( _color.value[2..4] );
						result ~= hexStringToByte( _color.value[4..6] );
						result ~= hexStringToByte( _color.value[6..8] );
					}
				}
			}

			return new IupString( cast(char[]) result.dup );
		}
		catch( Exception e )
		{
			debug IupMessage( "XPM:convert", toStringz( e.toString ) );
		}

		return null;
	}	
	/+
	static char*[] getXpm( char[] filePath )
	{
		try
		{
			scope file = new File( filePath, File.ReadExisting );

			int count;
			char*[] data;
			
			foreach( line; new Lines!(char)(file) )
			{
				if( count++ == 0 )
				{
					if( line != "/* XPM */" ) return null;
				}
				
				if( line.length )
				{
					if( line[0] == '"' )
					{
						int rPos = Util.rindex( line, "\"" );
						data ~= toStringz( line[1..rPos].dup );
					}					
				}
			}

			return data.dup;
		}
		catch( Exception e )
		{
			return null;
		}
	}
	+/
	static IupString getRGBA( char[] filePath )
	{
		try
		{
			return convert( filePath );
		}
		catch( Exception e )
		{
		}

		return null;
	}

	public:
	/* XPM */
	version(FBIDE)
	{
	static IupString private_fun_rgba, protected_fun_rgba, public_fun_rgba, private_sub_rgba, protected_sub_rgba, public_sub_rgba,
			private_variable_array_rgba, protected_variable_array_rgba, public_variable_array_rgba, private_variable_rgba, alias_obj_rgba,
			protected_variable_rgba, public_variable_rgba, class_private_obj_rgba, class_protected_obj_rgba,
			class_obj_rgba, struct_private_obj_rgba, struct_protected_obj_rgba, struct_obj_rgba,  
			union_private_obj_rgba,
			union_protected_obj_rgba, union_obj_rgba, enum_private_obj_rgba, enum_protected_obj_rgba, enum_obj_rgba,
			normal_rgba, with_rgba, parameter_rgba, enum_member_obj_rgba, template_obj_rgba,
			functionpointer_obj_rgba, namespace_obj_rgba, property_rgba, property_var_rgba, define_var_rgba, define_fun_rgba,
			bas_rgba, bi_rgba, folder_rgba,
			bookmark_rgba;
	}
	version(DIDE)
	{
	static IupString private_fun_rgba, protected_fun_rgba, public_fun_rgba, private_sub_rgba, protected_sub_rgba, public_sub_rgba,
			private_variable_array_rgba, protected_variable_array_rgba, public_variable_array_rgba, private_variable_rgba, alias_obj_rgba,
			protected_variable_rgba, public_variable_rgba, class_private_obj_rgba, class_protected_obj_rgba,
			interface_obj_rgba, class_obj_rgba, struct_private_obj_rgba, struct_protected_obj_rgba, struct_obj_rgba,  
			union_private_obj_rgba,
			union_protected_obj_rgba, union_obj_rgba, enum_private_obj_rgba, enum_protected_obj_rgba, enum_obj_rgba,
			normal_rgba, import_rgba, template_rgba, parameter_rgba, enum_member_obj_rgba,
			functionpointer_obj_rgba, namespace_obj_rgba, property_rgba, property_var_rgba, define_var_rgba, define_fun_rgba,
			bas_rgba, bi_rgba, folder_rgba,
			bookmark_rgba;
	}

	static Ihandle* getIUPimage( char[] filePath )
	{
		try
		{
			scope file = new File( filePath, File.ReadExisting );
			int 		count, colorSN, rPos;
			bool 		bPixel, bColor;
			int			quoteLineCount, width, height, num_colors, chars_per_pixel;
			char[]		prevLine, pixel;
			ColorUnit[]	color;
			
			
			
			foreach( int count, char[] line; new Lines!(char)(file) )
			{
				if( count == 0 )
				{
					if( line != "/* XPM */" ) return null;
				}
				
				if( line.length > 2 )
				{
					if( line[0..2] == "/*" || line[0] != '"' ) continue;
					
					if( line[0] == '"' ) quoteLineCount++;
					
					if( quoteLineCount == 1 )
					{
						char[]	formatString;
						bool	bIsNumber;
						foreach( char c; line[1..$-2] )
						{
							if( c > 47 && c < 58 )
							{
								formatString ~= c;
								bIsNumber = true;
							}
							else
							{
								if( bIsNumber )
								{
									bIsNumber = false;
									formatString ~= " ";
								}
							}
						}
						
						char[][] splitWords = Util.split( formatString, " " );
						if( splitWords.length > 3 )
						{
							width = Integer.atoi( splitWords[0] );
							height = Integer.atoi( splitWords[1] );
							num_colors = Integer.atoi( splitWords[2] );
							chars_per_pixel = Integer.atoi( splitWords[3] );
						}
						continue;
					}
					else if( quoteLineCount <= num_colors + 1 )
					{
						rPos = Util.rindex( line, "\"" );
						char[][] splitData = Util.split( line[1..rPos], " " );
						if( splitData.length == 3 )
						{
							ColorUnit _color;
							_color.index = splitData[0].dup;
							_color.c = splitData[1].dup;
							if( splitData[2] == "None" )
							{
								_color.value = "BGCOLOR";
							}
							else
							{
								int r = hexStringToByte( splitData[2][1..3] );
								int g = hexStringToByte( splitData[2][3..5] );
								int b = hexStringToByte( splitData[2][5..7] );

								_color.value = Integer.toString( r ) ~ " " ~ Integer.toString( g ) ~ " " ~ Integer.toString( b );
							}

							_color.sn = colorSN++;
							color ~= _color;
						}					
					
					}
					else if( quoteLineCount <= num_colors + 1 + width )
					{
						rPos = Util.rindex( line, "\"" );
						foreach( char c; line[1..rPos] )
							pixel ~= c;
					}
				}
			}			
			
			ubyte[] data; 
			foreach( char c; pixel )
			{
				foreach( ColorUnit __color; color )
				{
					if( c == __color.index[0] )
					{
						data ~= __color.sn;
						break;
					}
				}
			}
			
			Ihandle* image = IupImage( width, height, data.ptr );

			foreach( ColorUnit __color; color )
				IupSetStrAttribute( image, toStringz( Integer.toString( __color.sn ) ) , toStringz( __color.value ) );

			return image;

		}
		catch( Exception e )
		{
		}

		return null;
	}


	static void createIUPimageHandle( char[] filePath, char[] handleName, bool bCreateInvert = false )
	{
		try
		{
			scope file = new File( filePath, File.ReadExisting );
			int 		count, colorSN, rPos;
			bool 		bPixel, bColor;
			int			quoteLineCount, width, height, num_colors, chars_per_pixel;
			char[]		prevLine, pixel;
			ColorUnit[]	color;
			
			foreach( int count, char[] line; new Lines!(char)(file) )
			{
				if( count == 0 )
					if( line != "/* XPM */" ) return;
				
				if( line.length > 2 )
				{
					if( line[0..2] == "/*" || line[0] != '"' ) continue;
					
					if( line[0] == '"' ) quoteLineCount++;
					
					if( quoteLineCount == 1 )
					{
						char[]	formatString;
						bool	bIsNumber;
						foreach( char c; line[1..$-2] )
						{
							if( c > 47 && c < 58 )
							{
								formatString ~= c;
								bIsNumber = true;
							}
							else
							{
								if( bIsNumber )
								{
									bIsNumber = false;
									formatString ~= " ";
								}
							}
						}
						
						char[][] splitWords = Util.split( formatString, " " );
						if( splitWords.length > 3 )
						{
							width = Integer.atoi( splitWords[0] );
							height = Integer.atoi( splitWords[1] );
							num_colors = Integer.atoi( splitWords[2] );
							chars_per_pixel = Integer.atoi( splitWords[3] );
						}
						continue;
					}
					else if( quoteLineCount <= num_colors + 1 )
					{
						rPos = Util.rindex( line, "\"" );
						char[][] splitData = Util.split( line[1..rPos], " " );
						if( splitData.length == 3 )
						{
							ColorUnit _color;
							_color.index = splitData[0].dup;
							_color.c = splitData[1].dup;
							if( splitData[2] == "None" )
							{
								_color.value = "BGCOLOR";
							}
							else
							{
								int r = hexStringToByte( splitData[2][1..3] );
								int g = hexStringToByte( splitData[2][3..5] );
								int b = hexStringToByte( splitData[2][5..7] );
			
								if( GLOBAL.editorSetting00.IconInvert == "ALL" )
									_color.value = Integer.toString( 255 - r ) ~ " " ~ Integer.toString( 255 - g ) ~ " " ~ Integer.toString( 255 - b );
								else
									_color.value = Integer.toString( r ) ~ " " ~ Integer.toString( g ) ~ " " ~ Integer.toString( b );
							}

							_color.sn = colorSN++;
							color ~= _color;
						}					
					
					}
					else if( quoteLineCount <= num_colors + 1 + width )
					{
						rPos = Util.rindex( line, "\"" );
						foreach( char c; line[1..rPos] )
							pixel ~= c;
					}
				}
			}			
			
			ubyte[] data; 
			foreach( char c; pixel )
			{
				foreach( ColorUnit __color; color )
				{
					if( c == __color.index[0] )
					{
						data ~= __color.sn;
						break;
					}
				}
			}
			
			Ihandle* image = IupImage( width, height, data.ptr );

			foreach( ColorUnit __color; color )
				IupSetStrAttribute( image, toStringz( Integer.toString( __color.sn ) ) , toStringz( __color.value ) );
			
			auto _handleName = new IupString( handleName );
			IupSetHandle( _handleName.toCString, image );
			
			
			
			
			if( bCreateInvert )
			{
				Ihandle* imageInvert = IupImage( width, height, data.ptr );
				
				foreach( ColorUnit __color; color )
				{
					if( __color.value != "BGCOLOR" )
					{
						char[][] _colorValues = Util.split( __color.value, " " );
						if( _colorValues.length == 3 )
							__color.value = Integer.toString( 255 - Integer.atoi( _colorValues[0] ) ) ~ " " ~ Integer.toString( 255 - Integer.atoi( _colorValues[1] ) ) ~ " " ~ Integer.toString( 255 - Integer.atoi( _colorValues[2] ) );
					}
					IupSetStrAttribute( imageInvert, toStringz( Integer.toString( __color.sn ) ) , toStringz( __color.value ) );
					
					auto __handleName = new IupString( handleName ~ "_invert" );
					
					IupSetHandle( __handleName.toCString, imageInvert );
				}
			}

		}
		catch( Exception e )
		{
		}
	}

	static void init()
	{
		version(FBIDE)
		{
			private_fun_rgba 				= getRGBA( "icons/xpm/outline/fun_private.xpm");
			protected_fun_rgba 				= getRGBA( "icons/xpm/outline/fun_protected.xpm" );
			public_fun_rgba 				= getRGBA( "icons/xpm/outline/fun_public.xpm" );
			private_sub_rgba 				= getRGBA( "icons/xpm/outline/sub_private.xpm");
			protected_sub_rgba 				= getRGBA( "icons/xpm/outline/sub_protected.xpm" );
			public_sub_rgba 				= getRGBA( "icons/xpm/outline/sub_public.xpm" );
			
			private_variable_array_rgba		= getRGBA( "icons/xpm/outline/variable_array_private.xpm" );
			protected_variable_array_rgba 	= getRGBA( "icons/xpm/outline/variable_array_protected.xpm" );
			public_variable_array_rgba		= getRGBA( "icons/xpm/outline/variable_array.xpm" );
			private_variable_rgba 			= getRGBA( "icons/xpm/outline/variable_private.xpm" );
			protected_variable_rgba 		= getRGBA( "icons/xpm/outline/variable_protected.xpm" );
			public_variable_rgba 			= getRGBA( "icons/xpm/outline/variable.xpm" );
			
			class_private_obj_rgba 			= getRGBA( "icons/xpm/outline/class_private.xpm" );
			class_protected_obj_rgba		= getRGBA( "icons/xpm/outline/class_protected.xpm" );
			class_obj_rgba 					= getRGBA( "icons/xpm/outline/class.xpm" );
			
			struct_private_obj_rgba 		= getRGBA( "icons/xpm/outline/type_private.xpm" );
			struct_protected_obj_rgba 		= getRGBA( "icons/xpm/outline/type_protected.xpm" );
			struct_obj_rgba 				= getRGBA( "icons/xpm/outline/type.xpm" );

			union_private_obj_rgba			= getRGBA( "icons/xpm/outline/union_private.xpm" );
			union_protected_obj_rgba		= getRGBA( "icons/xpm/outline/union_protected.xpm" );
			union_obj_rgba					= getRGBA( "icons/xpm/outline/union.xpm" );
			
			enum_private_obj_rgba 			= getRGBA( "icons/xpm/outline//enum_private.xpm" );
			enum_protected_obj_rgba			= getRGBA( "icons/xpm/outline/enum_protected.xpm" );
			enum_obj_rgba					= getRGBA( "icons/xpm/outline/enum.xpm" );
			
			normal_rgba						= getRGBA( "icons/xpm/outline/normal.xpm" );
			//import_rgba					= getRGBA( "icons/xpm/import.xpm" );
			//autoWord_rgba					= getRGBA( "icons/xpm/autoword.xpm" );
			with_rgba						= getRGBA( "icons/xpm/outline/with.xpm" );
			
			parameter_rgba					= getRGBA( "icons/xpm/outline/parameter.xpm" );
			enum_member_obj_rgba			= getRGBA( "icons/xpm/outline/enum_member.xpm" );
			
			alias_obj_rgba					= getRGBA( "icons/xpm/outline/alias.xpm" );
			//functionpointer_obj_rgba		= getRGBA( "icons/xpm/functionpointer.xpm" );
			namespace_obj_rgba				= getRGBA( "icons/xpm/outline/namespace.xpm" );
			
			property_rgba					= getRGBA( "icons/xpm/outline/property.xpm" );
			property_var_rgba				= getRGBA( "icons/xpm/outline/property_var.xpm" );
			
			define_var_rgba					= getRGBA( "icons/xpm/outline/define_var.xpm" );
			define_fun_rgba					= getRGBA( "icons/xpm/outline/define_fun.xpm" );
			
			bas_rgba						= getRGBA( "icons/xpm/bas.xpm" );
			bi_rgba							= getRGBA( "icons/xpm/bi.xpm" );
			folder_rgba						= getRGBA( "icons/xpm/folder.xpm" );
		}
		version(DIDE)
		{
			private_fun_rgba 				= getRGBA( "icons/xpm/outline/fun_private.xpm");
			protected_fun_rgba 				= getRGBA( "icons/xpm/outline/fun_protected.xpm" );
			public_fun_rgba 				= getRGBA( "icons/xpm/outline/fun_public.xpm" );
			private_sub_rgba 				= getRGBA( "icons/xpm/outline/sub_private.xpm");
			protected_sub_rgba 				= getRGBA( "icons/xpm/outline/sub_protected.xpm" );
			public_sub_rgba 				= getRGBA( "icons/xpm/outline/sub_public.xpm" );
			
			private_variable_array_rgba		= getRGBA( "icons/xpm/outline/variable_array_private.xpm" );
			protected_variable_array_rgba 	= getRGBA( "icons/xpm/outline/variable_array_protected.xpm" );
			public_variable_array_rgba		= getRGBA( "icons/xpm/outline/variable_array.xpm" );
			private_variable_rgba 			= getRGBA( "icons/xpm/outline/variable_private.xpm" );
			protected_variable_rgba 		= getRGBA( "icons/xpm/outline/variable_protected.xpm" );
			public_variable_rgba 			= getRGBA( "icons/xpm/outline/variable.xpm" );
			
			class_private_obj_rgba 			= getRGBA( "icons/xpm/outline/class_private.xpm" );
			class_protected_obj_rgba		= getRGBA( "icons/xpm/outline/class_protected.xpm" );
			class_obj_rgba 					= getRGBA( "icons/xpm/outline/class.xpm" );
			struct_private_obj_rgba 		= getRGBA( "icons/xpm/outline/struct_private.xpm" );
			struct_protected_obj_rgba 		= getRGBA( "icons/xpm/outline/struct_protected.xpm" );
			struct_obj_rgba 				= getRGBA( "icons/xpm/outline/struct.xpm" );
			union_private_obj_rgba			= getRGBA( "icons/xpm/outline/union_private.xpm" );
			union_protected_obj_rgba		= getRGBA( "icons/xpm/outline/union_protected.xpm" );
			union_obj_rgba					= getRGBA( "icons/xpm/outline/union.xpm" );
			enum_private_obj_rgba 			= getRGBA( "icons/xpmoutline//enum_private.xpm" );
			enum_protected_obj_rgba			= getRGBA( "icons/xpm/outline/enum_protected.xpm" );
			enum_obj_rgba					= getRGBA( "icons/xpm/outline/enum.xpm" );
			
			normal_rgba						= getRGBA( "icons/xpm/outline/normal.xpm" );
			import_rgba						= getRGBA( "icons/xpm/outline/import.xpm" );
			template_rgba					= getRGBA( "icons/xpm/outline/template.xpm" );
			
			parameter_rgba					= getRGBA( "icons/xpm/outline/parameter.xpm" );
			enum_member_obj_rgba			= getRGBA( "icons/xpm/outline/enum_member.xpm" );
			
			alias_obj_rgba					= getRGBA( "icons/xpm/outline/alias.xpm" );
			interface_obj_rgba 				= getRGBA( "icons/xpm/outline/interface.xpm" );
			//functionpointer_obj_rgba		= getRGBA( "icons/xpm/functionpointer_obj.xpm" );
			namespace_obj_rgba				= getRGBA( "icons/xpm/outline/namespace.xpm" );
			
			property_rgba					= getRGBA( "icons/xpm/outline/property.xpm" );
			property_var_rgba				= getRGBA( "icons/xpm/outline/property_var.xpm" );
			
			define_var_rgba					= getRGBA( "icons/xpm/outline/define_var.xpm" );
			define_fun_rgba					= getRGBA( "icons/xpm/outline/define_fun.xpm" );
			
			bas_rgba						= getRGBA( "icons/xpm/d.xpm" );
			bi_rgba							= getRGBA( "icons/xpm/bi.xpm" );
			folder_rgba						= getRGBA( "icons/xpm/folder.xpm" );			
		}
		bookmark_rgba					= getRGBA( "icons/xpm/bookmark.xpm" );
	}	
}